extern "C" {
#include <stdint.h>
#include "inc/interrupts.h"
#include "driverlib/timer.h"
#include "inc/hw_memmap.h"
}

volatile bool TRIGGERED = false;
bool HIGH = false;

void HandleInterrupt()
{
    uint32_t status = TimerIntStatus(TIMER0_BASE, true);
    HIGH = !HIGH;
    TRIGGERED = true;
    TimerIntClear(TIMER0_BASE, status);
}
