/**
 * @brief main
 * Edited by Eric Li, Apr 10, 2022
 *
 * @par This file is the top level file for this project.  It executes all
 * initialization functions and provides the main loop for the program.
 *
 * @par This file is part of a CCS project and intended to be built with CCS.
 */


extern "C" {
    #include <stdint.h>
    #include "driverlib/timer.h"
    #include "inc/interrupts.h"
    #include "driverlib/sysctl.h"
    #include "driverlib/interrupt.h"
    #include "inc/hw_memmap.h"
    #include "driverlib/gpio.h"
    #include "inc/hw_ints.h"
}


#define BAUD 115200
#define RATE 1    // 10Hz?

#define HIGH_SIG 1
#define LOW_SIG 0

#define O_PIN GPIO_PIN_3   // port 3
#define BASE GPIO_PORTF_BASE      // BASE GPIO_PORTB_BASE

void Setup()
{
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypeGPIOOutput(BASE, O_PIN); // TODO change?

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
    TimerLoadSet(TIMER0_BASE, TIMER_A, (SysCtlClockGet() / RATE));
    TimerIntRegister(TIMER0_BASE, TIMER_A, &HandleInterrupt);
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_DMA | TIMER_TIMA_TIMEOUT);
    TimerEnable(TIMER0_BASE, TIMER_A);

    IntEnable(INT_TIMER0A);
    IntMasterEnable();
}

int main(void)
{
    // Boot Up
    Setup();

    // switch between high/low at 10Hz
    while (1)
    {
        if (TRIGGERED) {
            if (HIGH) {
                // write high
                GPIOPinWrite(BASE, O_PIN, O_PIN);
            } else {
                // write low
                GPIOPinWrite(BASE, O_PIN, LOW_SIG);
            }
            TRIGGERED = false;
        }
    }
}
